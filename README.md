# ExeBuilder

ExeBuilder 是一款利用 JDK 模块化的特性把jar打包成独立exe的工具，它支持GUI和控制台应用程序的创建。

ExeBuilder 1.0.4 下载:

链接：<a href="https://gitee.com/qsyan/ExeBuilder/raw/master/rar/ExeBuilder.rar" target="_blank">ExeBuilder.rar</a>  


![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20210915070353.png)  

![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20210915070411.png)  

![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20210915070501.png)  

**使用说明：**：  

将项目用 maven-dependency-plugin 和 maven-jar-plugin 打包成可执行jar和lib文件夹，桌面新建文件夹，比如app文件夹，将MainJar（可运行jar）和lib文件夹复制到app文件夹下，
此时，app目录就作为源目录。
安装 ExeBuilder，打开程序按步骤打包即可。

**注意：** 

1、打包后的exe文件所在的目录，自带一个模块化的JRE，可以使用innostep制作成安装包，部署到任意Windows上。  
2、运行本程序并不依赖JDK14,但是要打包exe文件，必须依赖JDK14+。请使用前先下载openjdk14，下载地址：https://download.java.net/openjdk/jdk14/ri/openjdk-14+36_windows-x64_bin.zip  
3、ExeBuilder安装在中文路径下，可能会导致一些问题，请安装在英文路径下。  
4、若要构建JavaFx应用程序，则最低支持JDK11构建的JavaFx应用程序，JDK8默认导出的jar包不包含JavaFx运行依赖（因为JDK8已经包含javafx运行依赖jar），打包exe后会缺少各种运行依赖。
5、暂不支持中文路径打包。
6、暂不支持Web应用打包。

**联系作者：**

QQ:331855843  
欢迎加入IDEA微社区QQ群：185441009


