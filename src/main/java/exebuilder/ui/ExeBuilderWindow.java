package exebuilder.ui;

import exebuilder.core.Windows;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ExeBuilderWindow extends Stage {
  private static final Logger logger = LogManager.getLogger();
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ExeBuilderWindow(String name) {
    initialize(name);
  }

  public void initialize(String name) {
    this.name = name;
    this.setOnCloseRequest(windowEvent -> dispose());
    getIcons().add(new Image(getClass().getResourceAsStream("/images/app.png")));
    Windows.add(name, this);
  }


  @Override
  public void close() {
    super.close();
    dispose();
  }

  public void dispose() {
    Windows.remove(this.getName());
    if (this.getName().equals("main")) {
      System.exit(0);
    }
  }
}
