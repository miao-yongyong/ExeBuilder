package exebuilder.core;

import exebuilder.ui.MessageBox;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ViewLoader {

 private static final Logger logger = LogManager.getLogger();

 public static Parent load(String fxml) {
  Parent view = null;
  try {
   view = FXMLLoader.load(ViewLoader.class.getResource("/view/" + fxml + ".fxml"));
  } catch (IOException e) {
   logger.error(e);
   Platform.runLater(() -> MessageBox.error(e.getMessage()));
  }
  return view;
 }
}
