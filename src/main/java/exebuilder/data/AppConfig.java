package exebuilder.data;

public class AppConfig {

 private static AppConfig appConfig;

 public Integer getAppType() {
  return appType;
 }

 public void setAppType(Integer appType) {
  this.appType = appType;
 }

 public String getAppName() {
  return appName;
 }

 public void setAppName(String appName) {
  this.appName = appName;
 }

 public String getOutputPath() {
  return outputPath;
 }

 public void setOutputPath(String outputPath) {
  this.outputPath = outputPath;
 }

 public Integer getJdkVersion() {
  return jdkVersion;
 }

 public void setJdkVersion(Integer jdkVersion) {
  this.jdkVersion = jdkVersion;
 }

 public String getJdkPath() {
  return jdkPath;
 }

 public void setJdkPath(String jdkPath) {
  this.jdkPath = jdkPath;
 }

 public String getSourcePath() {
  return sourcePath;
 }

 public void setSourcePath(String sourcePath) {
  this.sourcePath = sourcePath;
 }

 public String getMainJarFile() {
  return mainJarFile;
 }

 public void setMainJarFile(String mainJarFile) {
  this.mainJarFile = mainJarFile;
 }

 public String getMainClass() {
  return mainClass;
 }

 public void setMainClass(String mainClass) {
  this.mainClass = mainClass;
 }

 public String getLibPath() {
  return libPath;
 }

 public void setLibPath(String libPath) {
  this.libPath = libPath;
 }

 public String getIconPath() {
  return iconPath;
 }

 public void setIconPath(String iconPath) {
  this.iconPath = iconPath;
 }

 public String getVersion() {
  return version;
 }

 public void setVersion(String version) {
  this.version = version;
 }

 public String getCopyright() {
  return copyright;
 }

 public void setCopyright(String copyright) {
  this.copyright = copyright;
 }


 public String getTempDir() {
  return tempDir;
 }

 public void setTempDir(String tempDir) {
  this.tempDir = tempDir;
 }

 private String tempDir = "";

 private Integer appType = 0;    //默认GUI程序  1为控制台程序

 private String appName = "";

 private String outputPath = "";

 private Integer jdkVersion;

 private String jdkPath = "";

 private String sourcePath = "";

 private String mainJarFile = "";

 private String mainClass = "";

 private String libPath = "";

 private String iconPath = "";

 private String version = "";

 private String copyright = "";

 public static AppConfig getInstance() {
  if (appConfig == null) {
   appConfig = new AppConfig();
  }
  return appConfig;
 }

 @Override
 public String toString() {
  return "AppConfig{" +
         "tempDir='" + tempDir + '\'' +
         ", appType=" + appType +
         ", appName='" + appName + '\'' +
         ", outputPath='" + outputPath + '\'' +
         ", jdkVersion=" + jdkVersion +
         ", jdkPath='" + jdkPath + '\'' +
         ", sourcePath='" + sourcePath + '\'' +
         ", mainJarFile='" + mainJarFile + '\'' +
         ", mainClass='" + mainClass + '\'' +
         ", libPath='" + libPath + '\'' +
         ", iconPath='" + iconPath + '\'' +
         ", version='" + version + '\'' +
         ", copyright='" + copyright + '\'' +
         '}';
 }
}
